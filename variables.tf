variable "aws_region" {}
variable "root_domain_name" {}
variable "sub_domain" {}
variable "AWS_SECRET_ACCESS_KEY" {}
variable "AWS_ACCESS_KEY" {}